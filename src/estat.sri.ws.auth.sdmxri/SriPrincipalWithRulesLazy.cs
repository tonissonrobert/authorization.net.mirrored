// -----------------------------------------------------------------------
// <copyright file="SriPrincipalWithRulesLazy.cs" company="EUROSTAT">
//   Date Created : 2020-7-17
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Security.Principal;
using Estat.Sdmxsource.Extension.Constant;
using Estat.Sdmxsource.Extension.Engine;
using Estat.Sdmxsource.Extension.Manager;
using estat.sri.ws.auth.api;
using estat.sri.ws.auth.generic;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Estat.Sri.Utils.Helper;

namespace estat.sri.ws.auth.sdmxri
{
    internal class SriPrincipalWithRulesLazy : SriPrincipalWithRules
    {
        private readonly IDataflowAuthorizationManager _authorizationManager;

        /// <summary>
        ///     Store ID to
        /// </summary>
        private readonly ConcurrentDictionary<string, ArtefactReference[]> _authorizedRules =
            new ConcurrentDictionary<string, ArtefactReference[]>(StringComparer.Ordinal);

        private readonly IPrincipal _principal;

        public SriPrincipalWithRulesLazy(IPrincipal principal, IDataflowAuthorizationManager authorizationManager) :
            base(principal)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (authorizationManager == null)
            {
                throw new ArgumentNullException(nameof(authorizationManager));
            }

            _principal = principal;
            _authorizationManager = authorizationManager;
        }

        public override bool IsAuthorized(string storeId, ArtefactReference artefactReference, string permission)
        {
            if (storeId == null)
            {
                throw new ArgumentNullException(nameof(storeId));
            }

            if (artefactReference == null)
            {
                throw new ArgumentNullException(nameof(artefactReference));
            }

            if (permission == null)
            {
                throw new ArgumentNullException(nameof(permission));
            }

            if (!_principal.IsInRole(permission))
            {
                return false;
            }

            IStructureReference structureReference = artefactReference.ToStructureReference();
            // In case it is null
            if (structureReference == null)
            {
                return false;
            }

            PermissionType permissionType = PermissionsUtils.GetPermissionType(permission);
            // In the existing AuthDB we don't support any other permission than reading data, dataflow and categories
            // so if we are allowed to insert/update/delete we insert/update/delete

            switch(permissionType)
            {
                case PermissionType.CanDeleteStructuralMetadata:
                case PermissionType.CanImportStructures:
                case PermissionType.CanUpdateStructuralMetadata:
                    return true;
                case PermissionType.CanReadStructuralMetadata:
                    if (!(structureReference.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Dataflow ||
                        structureReference.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.CategoryScheme ||
                        structureReference.MaintainableStructureEnumType.EnumType == SdmxStructureEnumType.Categorisation))
                    {
                        return true;
                    }
                    break;
            }

            // TODO temporary solution, avoid using IDataflowAuthorization
            // The call below gets all dataflows and then filters them instead of checking access for one specific dataflow
            using (StoreContext storeContext = new StoreContext(storeId))
            {
                IDataflowAuthorizationEngine dataflowAuthorizationEngine = _authorizationManager.GetAuthorizationEngine(_principal);

                AuthorizationStatus status = dataflowAuthorizationEngine.GetAuthorization(structureReference,
                    permissionType);
                return status == AuthorizationStatus.Authorized;
            }
        }

        public override IList<ArtefactReference> GetArtefactReferences(string storeId, string permission)
        {
            if (storeId == null)
            {
                throw new ArgumentNullException(nameof(storeId));
            }

            if (permission == null)
            {
                throw new ArgumentNullException(nameof(permission));
            }

            if (!_principal.IsInRole(permission))
            {
                return new ArtefactReference[0];
            }

            // TODO temporary solution, avoid using IDataflowAuthorization
            return _authorizedRules.GetOrAdd(storeId, GetAuthorizedRules);
        }

        private ArtefactReference[] GetAuthorizedRules(string storeId)
        {
            // if it is admin return wildcard
            if (_principal.IsInRole("AdminRole"))
            {
                return new ArtefactReference[] { new ArtefactReference() { AgencyId = null, ArtefactType = "*", Id = null, Version = null } };
            }

            var artefactReferences = new List<ArtefactReference>();
            IList<IMaintainableRefObject> authorizedDataflows;
            using (StoreContext storeContext = new StoreContext(storeId))
            {
                IDataflowAuthorizationEngine dataflowAuthorizationEngine = _authorizationManager.GetAuthorizationEngine(_principal);
                authorizedDataflows = dataflowAuthorizationEngine.RetrieveAuthorizedDataflows();
            }

            foreach (IMaintainableRefObject authorizedDataflow in authorizedDataflows)
            {
                artefactReferences.Add(new ArtefactReference
                {
                    AgencyId = authorizedDataflow.AgencyId,
                    ArtefactType = SdmxStructureType.GetFromEnum(SdmxStructureEnumType.Dataflow).UrnClass,
                    Id = authorizedDataflow.MaintainableId,
                    Version = authorizedDataflow.Version
                });
            }

            // we need to allow all other artefacts as we have rules only for dataflows
            foreach(SdmxStructureType structureType in SdmxStructureType.MaintainableStructureTypes)
            {
                if (structureType.EnumType != SdmxStructureEnumType.Dataflow)
                {
                    artefactReferences.Add(new ArtefactReference
                    {
                        AgencyId = "*",
                        ArtefactType = structureType.UrnClass,
                        Id = "*",
                        Version = "*"
                    });
                }
            }

            return artefactReferences.ToArray();
        }

        public override bool IsInRole(string role)
        {
            return _principal.IsInRole(role);
        }
    }
}