// -----------------------------------------------------------------------
// <copyright file="DotStatProviderConfiguration.cs" company="EUROSTAT">
//   Date Created : 2020-7-17
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Db.Repository.SqlServer;
using DryIoc;
using estat.sri.ws.auth.api;
using Microsoft.Extensions.Configuration;

namespace estat.sri.ws.auth.dotstat
{
    public class DotStatProviderConfiguration : IRuleProviderConfiguration
    {
        public DotStatProviderConfiguration(IContainer container, IConfiguration configuration)
        {
            var ruleConfig = configuration.GetSection(RuleAuthorizationConfiguration.DefaultSection)?.Get<RuleAuthorizationConfiguration>();
            if (ruleConfig == null || !string.Equals("dotstat", ruleConfig.Method, StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            RegisterConfiguration(container, configuration);
            RegisterServices(container);
            RegisterRules(container);
        }

        private void RegisterRules(IContainer container)
        {
            container.Register<IUserAuthorizationRulesRetriever, DotStatUserAuthorizationRules>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Replace);
        }

        private static void RegisterConfiguration(IContainer container, IConfiguration configuration)
        {
            var authConfig = configuration.GetSection("auth").Get<AuthConfiguration>() ?? AuthConfiguration.Default;
            var baseConfig = configuration.Get<BaseConfiguration>();

            container.RegisterInstance(baseConfig);
            container.RegisterInstance<ILocalizationConfiguration>(baseConfig);
            container.RegisterInstance<IGeneralConfiguration>(baseConfig);
            container.RegisterInstance<IAuthConfiguration>(authConfig);
        }

        private static void RegisterServices(IContainer container)
        {
            container.Register<IAuthorizationRepository, SqlAuthorizationRepository>(reuse: Reuse.Singleton);
            container.Register<IAuthorizationManagement, AuthorizationManagement>(reuse: Reuse.Singleton);
        }

    }
}
