// -----------------------------------------------------------------------
// <copyright file="PermissionsUtils.cs" company="EUROSTAT">
//   Date Created : 2020-7-17
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using Estat.Sdmxsource.Extension.Constant;

namespace estat.sri.ws.auth.sdmxri
{
    internal static class PermissionsUtils
    {
        public static PermissionType GetPermissionType(string permission)
        {
            if (permission == null)
            {
                throw new ArgumentNullException(nameof(permission));
            }

            if (Enum.TryParse(permission, true, out PermissionType permissionType))
            {
                return permissionType;
            }

            // TODO avoid using DataflowAuthorizationManager and PermissionType
            throw new NotImplementedException($"Permission {permission} is not understood");
        }
    }
}