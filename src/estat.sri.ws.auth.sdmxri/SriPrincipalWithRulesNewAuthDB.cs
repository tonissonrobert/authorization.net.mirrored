// -----------------------------------------------------------------------
// <copyright file="SriPrincipalWithRulesNewAuthDB.cs" company="EUROSTAT">
//   Date Created : 2020-7-17
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or � as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Security.Model;
using estat.sri.ws.auth.api;

namespace estat.sri.ws.auth.sdmxri
{
    internal class SriPrincipalWithRulesNewAuthDB : SriPrincipalWithRulesLazy
    {
        private readonly SriPrincipal _principal;

        public SriPrincipalWithRulesNewAuthDB(SriPrincipal principal,
            IDataflowAuthorizationManager authorizationManager) : base(principal, authorizationManager)
        {
            _principal = principal;
        }

        public override bool IsAuthorized(string storeId, ArtefactReference artefactReference, string permission)
        {
            if (storeId == null)
            {
                throw new ArgumentNullException(nameof(storeId));
            }

            if (permission == null)
            {
                throw new ArgumentNullException(nameof(permission));
            }

            if (!_principal.CanAccessScope(storeId))
            {
                return false;
            }

            return base.IsAuthorized(storeId, artefactReference, permission);
        }

        public override IList<ArtefactReference> GetArtefactReferences(string storeId, string permission)
        {
            if (storeId == null)
            {
                throw new ArgumentNullException(nameof(storeId));
            }

            if (permission == null)
            {
                throw new ArgumentNullException(nameof(permission));
            }

            if (!_principal.CanAccessScope(storeId))
            {
                return new ArtefactReference[0];
            }

            return base.GetArtefactReferences(storeId, permission);
        }
    }
}