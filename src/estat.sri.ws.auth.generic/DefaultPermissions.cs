namespace estat.sri.ws.auth.generic
{
    /// <summary>
    ///     Default basic permissions.
    ///     Not enum to allow changing them without recompiling e.g. from a json file
    /// </summary>
    public static class DefaultPermissions
    {
        public static string CanReadStructuralMetadata { get; set; } = "CanReadStructuralMetadata";

        /// <summary>
        ///     The can read data attribute
        /// </summary>
        public static string CanReadData { get; set; } = "CanReadData";

        /// <summary>
        ///     The can read point in time data attribute
        /// </summary>
        public static string CanReadPitData { get; set; } = "CanReadPitData";

        /// <summary>
        ///     The can ignore production flag attribute
        /// </summary>
        public static string CanIgnoreProductionFlag { get; set; } = "CanIgnoreProductionFlag";

        /// <summary>
        ///     The can perform internal mapping configuration attribute
        /// </summary>
        public static string CanPerformInternalMappingConfig { get; set; } = "CanPerformInternalMappingConfig";

        /// <summary>
        ///     The can import structures attribute
        /// </summary>
        public static string CanImportStructures { get; set; } = "CanImportStructures";

        /// <summary>
        ///     The can import data attribute
        /// </summary>
        public static string CanImportData { get; set; } = "CanImportData";

        /// <summary>
        ///     The can modify store settings attribute
        /// </summary>
        public static string CanModifyStoreSettings { get; set; } = "CanModifyStoreSettings";

        /// <summary>
        ///     Can update structural metadata attribute
        /// </summary>
        public static string CanUpdateStructuralMetadata { get; set; } = "CanUpdateStructuralMetadata";

        /// <summary>
        ///     Can update data
        /// </summary>
        public static string CanUpdateData { get; set; } = "CanUpdateData";

        /// <summary>
        ///     Can delete structural metadata attribute
        /// </summary>
        public static string CanDeleteStructuralMetadata { get; set; } = "CanDeleteStructuralMetadata";

        /// <summary>
        ///     Can delete data
        /// </summary>
        public static string CanDeleteData { get; set; } = "CanDeleteData";
    }
}