using System;
using System.Collections.Generic;
using System.Security.Principal;
using estat.sri.ws.auth.api;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace estat.sri.ws.auth.generic
{
    public class GenericAuthorizationDefault : IGenericAuthorization
    {
        public bool CanReadData(IPrincipal principal, string storeId, IStructureReference dataflow, bool isPit = false)
        {
            var principalWithRules = principal as SriPrincipalWithRules;
            if (principalWithRules == null)
            {
                return false;
            }

            var dataflowReference = dataflow.ToArtefactReference();
            var canReadData = principalWithRules.IsAuthorized(storeId, dataflowReference, DefaultPermissions.CanReadData);

            if (!isPit)
            {
                return canReadData;
            }

            // Point in time request.
            return canReadData && principalWithRules.IsAuthorized(storeId, dataflowReference, DefaultPermissions.CanImportData) ||
                   principalWithRules.IsAuthorized(storeId, dataflowReference, DefaultPermissions.CanReadPitData);
        }

        public bool CanReadStructure(IPrincipal principal, string storeId, IStructureReference artefact)
        {
            var principalWithRules = principal as SriPrincipalWithRules;
            if (principalWithRules == null)
            {
                return false;
            }

            return principalWithRules.IsAuthorized(storeId, artefact.ToArtefactReference(), DefaultPermissions.CanReadStructuralMetadata);
        }

        public ISet<IStructureReference> GetReadableArtefacts(IPrincipal principal, string storeId)
        {
            var principalWithRules = principal as SriPrincipalWithRules;
            if (principalWithRules == null)
            {
                return new HashSet<IStructureReference>();
            }

            var set = new HashSet<IStructureReference>();
            var userArtefacts = principalWithRules.GetArtefactReferences(storeId, DefaultPermissions.CanReadStructuralMetadata);
            foreach (var userArtefact in userArtefacts)
            {
                IStructureReference structureReference = userArtefact.ToStructureReference();
                // ignore entries that we don't know how to map to SdmxStuctureType
                if (structureReference != null)
                {
                    set.Add(structureReference);
                }
            }

            return set;
        }

        public bool IsAuthorized(IPrincipal principal, string storeId, IMaintainableObject artefact,
            StructureAction action)
        {
            var principalWithRules = principal as SriPrincipalWithRules;
            if (principalWithRules == null)
            {
                return false;
            }

            var artefactReference = artefact.ToArtefactReference();
            string requiredPermission;
            switch (action)
            {
                case StructureAction.Insert:
                    requiredPermission = DefaultPermissions.CanImportStructures;
                    break;
                case StructureAction.UpdateFinal:
                    requiredPermission = DefaultPermissions.CanUpdateStructuralMetadata;
                    break;
                case StructureAction.ReplaceNonFinal:
                    // TODO do we need special permission for this ?
                    requiredPermission = DefaultPermissions.CanUpdateStructuralMetadata;
                    break;
                case StructureAction.Delete:
                    requiredPermission = DefaultPermissions.CanDeleteStructuralMetadata;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, "Unsupported Action");
            }

            return principalWithRules.IsAuthorized(storeId, artefactReference, requiredPermission);
        }
    }
}