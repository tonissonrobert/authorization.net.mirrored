// -----------------------------------------------------------------------
// <copyright file="DotStatUserAuthorizationRules.cs" company="EUROSTAT">
//   Date Created : 2020-7-17
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Enums;
using DotStat.Common.Model;
using estat.sri.ws.auth.api;
using Microsoft.AspNetCore.Http;
using Org.Sdmxsource.Sdmx.Api.Constants;
using PermissionType = Estat.Sdmxsource.Extension.Constant.PermissionType;

namespace estat.sri.ws.auth.dotstat
{
    internal class DotStatUserAuthorizationRules : IUserAuthorizationRulesRetriever
    {
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IAuthorizationManagement _authorizationManagement;

        public DotStatUserAuthorizationRules(IAuthorizationManagement authorizationManagement,
            IAuthConfiguration authConfiguration, IHttpContextAccessor ctx)
        {
            if (authorizationManagement == null)
            {
                throw new ArgumentNullException(nameof(authorizationManagement));
            }

            if (authConfiguration == null)
            {
                throw new ArgumentNullException(nameof(authConfiguration));
            }

            _authorizationManagement = authorizationManagement;
            _authConfiguration = authConfiguration;

            DotStat.Common.Logger.Log.Configure(ctx);
        }

        public SriPrincipalWithRules GetUserWithRules(IPrincipal principal)
        {
            // TODO Check if this is the correct approach
            ClaimsPrincipal claims = principal as ClaimsPrincipal;
            if (principal == null)
            {
                return null;
            }

            var dotStatPrincipal =
                new DotStatPrincipal(claims, _authConfiguration.ClaimsMapping);

            // TODO check if anonymous user is covered (anonymous user has a ClaimsPrincipal at least in my tests).
            // We get all the rules because we need IsInRole functionality.
            IEnumerable<UserAuthorization> rules = _authorizationManagement.UserRules(dotStatPrincipal);
            List<UserAuthorizationRules> userAuthorizationRules = new List<UserAuthorizationRules>();
            foreach (UserAuthorization rule in rules)
            {
                var authorizationRules = new UserAuthorizationRules {StoreId = rule.DataSpace};
                authorizationRules.Artefact.Id = rule.ArtefactId;
                authorizationRules.Artefact.AgencyId = rule.ArtefactAgencyId;
                authorizationRules.Artefact.Version = rule.ArtefactVersion;

                string artefactType = ConvertArtefactType(rule.ArtefactType);

                authorizationRules.Artefact.ArtefactType = artefactType;

                // (Workaround) Adds all permissions that and actual rule permission covers (e.g. AdminRole role has all) as in SriPrincipalWithRules class exact string match is checked
                authorizationRules.Permissions.UnionWith(((PermissionType[])Enum.GetValues(typeof(PermissionType)))
                    .Where(p =>
                        p != PermissionType.None && ((p & rule.Permission) == p)).Select(p => p.ToString()));

                userAuthorizationRules.Add(authorizationRules);
            }

            return new SriPrincipalWithRules(dotStatPrincipal, userAuthorizationRules);
        }

        private static string ConvertArtefactType(SDMXArtefactType sdmxArtefactType)
        {
            // rule.ArtefactType enum seems to be related to SdmxSource SdmxStructureEnumType (.NET naming)
            // So we map from SDMXArtefactType to SdmxStuctureEnumType and get the SdmxStructureType UrnClass
            // but in case SDMXArtefactType cant translate to SDMX Structure Type, we will use the value as it is
            // to avoid blocking the request
            string artefactType;
            string ruleSDMXArtefactType = sdmxArtefactType.ToString();
            if (Enum.TryParse(ruleSDMXArtefactType, true,
                    out SdmxStructureEnumType enumType) && enumType != SdmxStructureEnumType.Any &&
                enumType != SdmxStructureEnumType.Null)
            {
                artefactType = SdmxStructureType.GetFromEnum(enumType).UrnClass;
            }
            else
            {
                // Non-SDMX type
                artefactType = ruleSDMXArtefactType;
            }

            return artefactType;
        }
    }
}