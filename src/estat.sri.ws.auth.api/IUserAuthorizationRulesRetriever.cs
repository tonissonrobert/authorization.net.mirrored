// -----------------------------------------------------------------------
// <copyright file="IUserAuthorizationRulesRetriever.cs" company="EUROSTAT">
//   Date Created : 2020-7-13
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Security.Principal;

namespace estat.sri.ws.auth.api
{
    /// <summary>
    ///     Interface for getting a principal with access to user rules
    /// </summary>
    public interface IUserAuthorizationRulesRetriever
    {
        /// <summary>
        ///     Gets an instance of <see cref="SriPrincipalWithRules" /> that can access the user authorization rules
        /// </summary>
        /// <param name="principal">The authenticated (or anonymous) principal</param>
        /// <returns>The <see cref="SriPrincipalWithRules" /></returns>
        SriPrincipalWithRules GetUserWithRules(IPrincipal principal);
    }
}