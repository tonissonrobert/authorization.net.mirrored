using System;
using estat.sri.ws.auth.api;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;

namespace estat.sri.ws.auth.generic
{
    public static class ArtefactReferenceExtensions
    {
        public static ArtefactReference ToArtefactReference(this IStructureReference structureReference)
        {
            var reference = new ArtefactReference();
            if (structureReference.HasAgencyId())
            {
                reference.AgencyId = structureReference.AgencyId;
            }
           
            if (structureReference.HasMaintainableId())
            {
                reference.Id = structureReference.MaintainableId;
            }

            if (structureReference.HasVersion())
            {
                reference.Version = structureReference.Version;
            }

            reference.ArtefactType = structureReference.MaintainableStructureEnumType.UrnClass;
            return reference;
        }

        public static ArtefactReference ToArtefactReference(this IMaintainableObject structure)
        {
            return new ArtefactReference
            {
                AgencyId = structure.AgencyId,
                Id = structure.Id,
                Version = structure.Version,
                ArtefactType = structure.StructureType.UrnClass
            };
        }

        /// <summary>
        ///     Convert the specified <paramref name="artefactReference" /> to a SdmxSource <see cref="IStructureReference" />
        /// </summary>
        /// <param name="artefactReference">The artefact reference</param>
        /// <returns>
        ///     If the <see cref="ArtefactReference.ArtefactType" /> is recognised then a <see cref="IStructureReference" />
        ///     with the same information. Else <c>null</c>
        /// </returns>
        public static IStructureReference ToStructureReference(this ArtefactReference artefactReference)
        {
            // TODO support categories
            // Wildcard
            if (string.Equals(artefactReference.ArtefactType, "*") || string.Equals(artefactReference.ArtefactType,
                "Any", StringComparison.OrdinalIgnoreCase))
            {
                return new StructureReferenceImpl(artefactReference.AgencyId, artefactReference.Id,
                    artefactReference.Version, SdmxStructureEnumType.Any);
            }

            // Not all ArtefactType correspond to an SDMX structure
            // So we cannot use SdmxStructureType.ParseClass the following as it throws an exception
            foreach (var sdmxStructureType in SdmxStructureType.Values)
            {
                if (sdmxStructureType.IsIdentifiable
                    && (sdmxStructureType.UrnClass.Equals(artefactReference.ArtefactType,
                            StringComparison.OrdinalIgnoreCase)
                        || sdmxStructureType.V2Class != null
                        && sdmxStructureType.V2Class.Equals(artefactReference.ArtefactType,
                            StringComparison.OrdinalIgnoreCase)))
                {
                    return new StructureReferenceImpl(WildCardToNull(artefactReference.AgencyId), WildCardToNull(artefactReference.Id),
                        WildCardToNull(artefactReference.Version), sdmxStructureType);
                }
            }

            // we don't have a known artefact type. NSIWS wont know how to handle this
            // throwing an exception will block the request (unless used as a control flow)
            // so we return null
            return null;
        }

        private static string WildCardToNull(string value)
        {
            if ("*".Equals(value))
            {
                return null;
            }

            return value;
        }
    }
}