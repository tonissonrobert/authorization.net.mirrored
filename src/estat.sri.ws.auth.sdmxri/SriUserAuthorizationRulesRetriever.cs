// -----------------------------------------------------------------------
// <copyright file="SriUserAuthorizationRulesRetriever.cs" company="EUROSTAT">
//   Date Created : 2020-7-17
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System.Security.Principal;
using Estat.Sdmxsource.Extension.Manager;
using Estat.Sri.Security.Model;
using estat.sri.ws.auth.api;

namespace estat.sri.ws.auth.sdmxri
{
    internal class SriUserAuthorizationRulesRetriever : IUserAuthorizationRulesRetriever
    {
        private readonly IDataflowAuthorizationManager _authorizationManager;

        public SriUserAuthorizationRulesRetriever(IDataflowAuthorizationManager authorizationManager)
        {
            _authorizationManager = authorizationManager;
        }

        public SriPrincipalWithRules GetUserWithRules(IPrincipal principal)
        {
            if (principal == null)
            {
                // TODO what could happen here
                return null;
            }

            if (principal is SriPrincipalWithRules sriPrincipalWithRules)
            {
                return sriPrincipalWithRules;
            }

            if (principal is SriPrincipal sriPrincipal)
            {
                return new SriPrincipalWithRulesNewAuthDB(sriPrincipal, _authorizationManager);
            }

            return new SriPrincipalWithRulesLazy(principal, _authorizationManager);
        }
    }
}