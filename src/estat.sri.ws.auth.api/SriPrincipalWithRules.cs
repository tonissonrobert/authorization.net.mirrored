// -----------------------------------------------------------------------
// <copyright file="SriPrincipalWithRules.cs" company="EUROSTAT">
//   Date Created : 2020-7-13
//   Copyright (c) 2009, 2020 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using Org.Sdmxsource.Sdmx.Api.Constants;

namespace estat.sri.ws.auth.api
{
    public class SriPrincipalWithRules : ClaimsPrincipal
    {
        private static readonly string WildCard = "*";
        private readonly UserAuthorizationRules[] rules;

        /// <summary>
        ///     Initialize a new instance of the <see cref="SriPrincipalWithRules" /> class
        /// </summary>
        /// <param name="principal">The current principal</param>
        /// <param name="rules">the user rules</param>
        public SriPrincipalWithRules(IPrincipal principal, IEnumerable<UserAuthorizationRules> rules) : base(principal)
        {
            if (principal is null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (rules is null)
            {
                throw new ArgumentNullException(nameof(rules));
            }

            this.rules = rules.ToArray();
        }

        /// <summary>
        ///     Used only in cases we override the principal
        /// </summary>
        protected SriPrincipalWithRules(IPrincipal principal) : base(principal)
        {
            rules = new UserAuthorizationRules[0];
        }

        /// <summary>
        ///     Gets a value indicating whether the current <see cref="SriPrincipalWithRules" /> can access the specified artefact
        /// </summary>
        /// <param name="storeId">The requested Mapping Store or DataSpace ID</param>
        /// <param name="artefactReference">The requested artefact reference</param>
        /// <param name="permission">The requested permission</param>
        /// <returns>True if this user is authorized to access the resouce; else false</returns>
        public virtual bool IsAuthorized(string storeId, ArtefactReference artefactReference, string permission)
        {
            if (string.IsNullOrEmpty(storeId))
            {
                throw new ArgumentException($"'{nameof(storeId)}' cannot be null or empty", nameof(storeId));
            }

            if (string.Equals(WildCard, storeId))
            {
                throw new ArgumentException($"'{nameof(storeId)}' cannot equal to {WildCard}", nameof(storeId));
            }

            if (artefactReference is null)
            {
                throw new ArgumentNullException(nameof(artefactReference));
            }

            if (string.IsNullOrEmpty(permission))
            {
                throw new ArgumentException($"'{nameof(permission)}' cannot be null or empty", nameof(permission));
            }

            // todo optimize
            foreach (var rule in rules)
            {
                if (IsMatch(rule.StoreId, storeId) && IsMatch(rule.Artefact, artefactReference) &&
                    rule.Permissions.Contains(permission))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        ///     Gets a list of SDMX artefact references that this <see cref="SriPrincipalWithRules" /> is allowed to access
        /// </summary>
        /// <param name="storeId">The mapping store Id or data space</param>
        /// <param name="permission">The requested permission</param>
        /// <returns>The list of authorized artefact references</returns>
        /// <exception cref="ArgumentException">StoreId is null or empty or equals to "*"</exception>
        public virtual IList<ArtefactReference> GetArtefactReferences(string storeId, string permission)
        {
            if (string.IsNullOrEmpty(storeId))
            {
                throw new ArgumentException($"'{nameof(storeId)}' cannot be null or empty", nameof(storeId));
            }

            if (string.IsNullOrEmpty(permission))
            {
                throw new ArgumentException($"'{nameof(permission)}' cannot be null or empty", nameof(permission));
            }

            if (string.Equals(WildCard, storeId))
            {
                throw new ArgumentException($"'{nameof(storeId)}' cannot equal to {WildCard}", nameof(storeId));
            }


            var artefactRefeferences = new List<ArtefactReference>();
            foreach (var rule in rules)
            {
                if (IsMatch(rule.StoreId, storeId))
                {
                    if (rule.Permissions.Contains(permission))
                    {
                        artefactRefeferences.Add(rule.Artefact);
                    }
                }
            }

            return artefactRefeferences;
        }

        /// <inheritdoc />
        public override bool IsInRole(string role)
        {
            foreach (var rule in rules)
            {
                if (rule.Permissions.Contains(role))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool IsMatch(ArtefactReference source, ArtefactReference requested)
        {
            return IsMatchOfArtefactType(source.ArtefactType, requested.ArtefactType) &&
                   IsMatch(source.AgencyId, requested.AgencyId) &&
                   IsMatch(source.Id, requested.Id) &&
                   IsMatch(source.Version, requested.Version);
        }

        private static bool IsMatch(string source, string requested)
        {
            return string.IsNullOrEmpty(source) || string.Equals(source, WildCard) || string.Equals(source, requested, StringComparison.OrdinalIgnoreCase);
        }

        private static bool IsMatchOfArtefactType(string source, string requested)
        {
            return string.Equals(source, WildCard) || string.Equals(source, SdmxStructureEnumType.Any.ToString(), StringComparison.OrdinalIgnoreCase) || string.Equals(source, requested, StringComparison.OrdinalIgnoreCase);
        }
    }
}