namespace estat.sri.ws.auth.generic
{
    public enum StructureAction
    {
        Insert,
        UpdateFinal,
        ReplaceNonFinal,
        Delete
    }
}