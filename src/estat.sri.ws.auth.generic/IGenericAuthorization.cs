using System.Collections.Generic;
using System.Security.Principal;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Reference;

namespace estat.sri.ws.auth.generic
{
    public interface IGenericAuthorization
    {
        /// <summary>
        /// Gets a value indicating whether the specified <paramref name="principal"/> can read the <paramref name="dataflow"/> on <paramref name="storeId"/>
        /// </summary>
        /// <param name="principal">The current user</param>
        /// <param name="storeId">The mapping store id/data space</param>
        /// <param name="dataflow">The requested dataflow. Used <see cref="IStructureReference"/> to possibly support PA/DSD</param>
        /// <param name="isPit">Gets or sets a value indicating whether it is a point in time query.</param>
        /// <returns>True if the user is authorized; otherwise false</returns>
        bool CanReadData(IPrincipal principal, string storeId, IStructureReference dataflow, bool isPit = false);

        /// <summary>
        /// Gets a value indicating whether the specified <paramref name="principal"/> can read the <paramref name="artefact"/> on <paramref name="storeId"/>
        /// </summary>
        /// <param name="principal">The current user</param>
        /// <param name="storeId">The mapping store id/data space</param>
        /// <param name="artefact">The requested artefact.</param>
        /// <returns>True if the user is authorized; otherwise false</returns>
        bool CanReadStructure(IPrincipal principal, string storeId, IStructureReference artefact);

        /// <summary>
        /// Gets the list of of artefacts the <paramref name="principal"/> can access on <paramref name="storeId"/>
        /// </summary>
        /// <param name="principal">The current user</param>
        /// <param name="storeId">The mapping store id/data space</param>
        /// <returns>The distinct list of structure references. May contain wildcards</returns>
        ISet<IStructureReference> GetReadableArtefacts(IPrincipal principal, string storeId);

        /// <summary>
        /// Gets a value indicate whether the <paramref name="principal"/> has access to perform <paramref name="action"/> on <paramref name="storeId"/> for <paramref name="artefact"/>
        /// </summary>
        /// <param name="principal">The current user</param>
        /// <param name="storeId">The mapping store id/data space</param>
        /// <param name="artefact">The submitted artefact.</param>
        /// <param name="action">The submit action</param>
        /// <returns></returns>
        bool IsAuthorized(IPrincipal principal, string storeId, IMaintainableObject artefact, StructureAction action);
    }
}