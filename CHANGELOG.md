## authorization.net v8.1.0 (2020-10-16)

### Details v8.1.0

Initial release with SDMXRI AUTHDB and DoStat plugins

### Tickets v8.1.0

The following new features added:

- SDMXRI-1340: SDMXRI use AuthManagement web service (OECD)

